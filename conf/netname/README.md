# netname network

This network is a 3 node network, where we are node0, and are peered with node1
and node2. We only attempt to connect to node1, but still want node2 to be able
to NAT punch to us (node2 will be configured to connect to us).

The network is 10.0.0.0/22. Node0 will take 10.0.0.0/24, node1 will take
10.0.1.0/24, and node2 will take 10.0.2.0/24. All keys have been removed.
