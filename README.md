# uctinc/tinc

This is a complimentary image for running tinc within a Docker container. This
container is:

+ alpine based
+ running tinc 1.0.34, the latest stable version

This container requires local configuration of a share to be used for its
configuration. A good guide for configuration can be found [in the tinc
documentation](https://www.tinc-vpn.org/documentation/Configuration.html#Configuration).

The syntax for the container is as follows:

```
docker run gitlab.com/uctinc/tinc:master -p 655:655 -v <host conf dir>:/etc/tinc
<netname> <other configuration>
```

You do not have to specify -n to specify the net name. For example, when
running, you can simply run the following:

```
docker run gitlab.com/uctinc/tinc:master -p 655:655 -v <host conf dir>:/etc/tinc
<netname>
```

Where netname is the name of the network you are connecting to.

Generation of keys works by running the container in interactive mode and adding
the K flag, as shown:

```
docker run --rm -it gitlab.com/uctinc/tinc:master -p 655:655 -v <host conf dir>:/etc/tinc
<netname> -K
```

An example configuration is located in the [conf/](./conf) directory of this
repository. This example will purposefully not work on UCTINC! Please validate
your configuration before using.

As always, the [ArchLinux wiki](https://wiki.archlinux.org/index.php/Tinc) is a
great general purpose example for how to do this.
