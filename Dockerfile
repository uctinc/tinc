FROM alpine:latest

RUN apk --no-cache add tinc

EXPOSE 655
ENTRYPOINT ["tincd", "-D", "-n"]
